# BEGIN ctwmrc
# CTWM - "3D" / Carl Svensson / www.datagubbe.se / version 2.12

# Misc flags
DontShowWelcomeWindow
NoGrabServer
RestartPreviousState
DecorateTransients
DontMoveOff
RandomPlacement
NoOpaqueResize
IconifyByUnmapping
MoveDelta 5
NoSortIconManager
AutoFocusToTransients
NoMenuShadows
StayUpMenus

UseThreeDMenus
UseThreeDTitles
UseThreeDIconManagers
UseThreeDBorders
UseThreeDWMap
SunkFocusWindowTitle
TitleJustification "center"

# Fonts
TitleFont       "-*-helvetica-bold-r-normal-*-17-*-*-*-*-*-iso10646-1"
ResizeFont      "-*-helvetica-bold-r-normal-*-17-*-*-*-*-*-iso10646-1"
MenuFont        "-*-helvetica-bold-r-normal-*-17-*-*-*-*-*-iso10646-1"
IconFont        "-*-helvetica-bold-r-normal-*-14-*-*-*-*-*-iso10646-1"
IconManagerFont "-*-helvetica-bold-r-normal-*-14-*-*-*-*-*-iso10646-1"

# Custom colours
Color {
  TitleBackground       "CadetBlue"
  TitleForeground       "#0F2F2F"
  BorderColor           "PaleVioletRed"
  BorderTileBackground  "CadetBlue"
  BorderTileForeground  "CadetBlue"

  MenuBackground        "Gray"
  MenuForeground        "Black"
  MenuTitleBackground   "#686B9F"
  MenuTitleForeground   "White"

  IconManagerBackground "Gray"
  IconManagerForeground "Black"
}

# Handle Window ring
WindowRing
WindowRingExclude {
  "xload"
  "xclock"
}

# Set custom cursors
Cursors {
  Frame       "left_ptr"
  Title       "left_ptr"
  Icon        "left_ptr"
  IconMgr     "hand2"
  Move        "fleur"
  Resize      "iron_cross"
  Menu        "sb_left_arrow"
  Button      "hand2"
  Wait        "watch"
  Select      "dot"
  Destroy     "pirate"
}

# Icon manager configuration
ShowIconManager
IconManagerGeometry "119-0+42"
IconManagerDontShow {
  "gkrellm"
  "xclock"
  "xload"
  "sysinf2"
}


# Turn off some window titles
NoTitle {
  "TWM Icon Manager"
  "gkrellm"
  "xload"
  "xclock"
  "sysinf2"
  "feh"
  "Cell"
  "VIC-II"
  "xeyes"
}

# Custom functions
Function "move-and-raise" {
  f.move
  f.deltastop
  f.raise
}
Function "deiconify-and-raise" {
  f.deiconify
  f.raise
}
Function "next-and-raise" {
  f.downiconmgr
  f.raise
}

# Turn off default iconify and resize button
NoDefaults
# Custom Titlebuttons
#   Bitmaps are stored in /usr/include/X11/bitmaps
#   Built-in options:
#   :dot, :xlogo, :iconify, :resize, :question, :delete, :menu
LeftTitleButton  ":xpm:bar"    = f.menu "winmops"
RightTitleButton ":xpm:dot"    = f.iconify
RightTitleButton ":xpm:box"    = f.zoom
RightTitleButton ":xpm:resize" = f.resize

# Mouse bindings
Button3 = : root : f.menu "defops"
Button1 = : root : f.menu "apps"
Button2 = : root : f.menu "TwmWindows"

Button1 = : title : f.function "move-and-raise"
Button2 = : title : f.iconify
Button3 = : title : f.raiselower

Button1 = : frame : f.function "move-and-raise"
Button2 = : frame : f.resize
Button3 = : frame : f.raiselower

Button1 = : iconmgr : f.function "deiconify-and-raise"
Button2 = : iconmgr : f.lower
Button3 = : iconmgr : f.iconify

# Keyboard Bindings
"F1"  = m4 : all : f.fullzoom
"F2"  = m4 : all : f.zoom
"Tab" = m   : all : f.warpring "next"
"Tab" = m|s : all : f.warpring "prev"

# Meta key + mouse actions, useful for title-less windows
Button1 = m4 : window : f.forcemove
Button3 = m4 : window : f.raiselower
Button2 = m4 : window : f.resize

# Custom title bar pixmap
Pixmaps {
  TitleHighlight ".config/twmtitle4.xbm"
}

# Small menu for window "close" button
menu "winmops"
{
  "Iconify" f.iconify
  "Close"   f.delete
}

# System menu (Right click in root)
menu "defops"
{
  "System"     f.title
  "Resize"     f.resize
  "Move"       f.move
  ""           f.nop
  "Kill"       f.destroy
  "Delete"     f.delete
  ""           f.nop
  "SysLog"     f.exec "xterm -fn 7x14 -geometry 130x40 -cr blue -bg white -fg darkslategray -title syslog -e tail -f /var/log/syslog &"
  "SysTemp"    f.exec "psensor &"
  ""           f.nop
  "Sleep"      f.menu "asksleep"
  #"Lock"       f.exec "dm-tool lock &"
  ""           f.nop
  "Restart"    f.restart
  "Logout"       f.menu "askexit"
}

# System submenu: Confirm sleep mode
menu "asksleep"
{
  "Sleep?" f.title
  "No"     f.nop
  "Yes"    f.exec "systemctl suspend"
}

# System submenu: Confirm exit
menu "askexit"
{
  "Exit?" f.title
  "No"    f.nop
  "Yes"   f.quit
}

# Workspace Menu (Left click in root)
menu "apps"
{
  "Workspace"     f.title
  "Terminal Emulator"         f.exec "uxterm &"
  "VIsual Editor"  f.exec "uxterm -geometry 88x40 -T VI -e vi &"
  "VIsual iMproved Editor"  f.exec "uxterm -geometry 88x40 -T VIM -e vim &"
  "NeoVIsual iMproved Editor"  f.exec "uxterm -geometry 88x40 -T NeoVIM -e nvim &"
  #"Small XTerm"   f.exec "xterm &"
  #""              f.nop
  #"Links2"        f.exec "links2 -g &"
  #"Sylpheed"      f.exec "sylpheed &" #"TubeShell"     f.exec "xterm -fn 7x14 -geometry 90x26 -title tube -e tubeshell &"
  #"Grafx2"        f.exec "grafx2&" 
  #"Slack-Term"    f.exec "xterm -fn 7x14 -geometry 177x58 -title slack-term -e slack-term &"
  #"WordGrinder"   f.exec "xterm -title wordgrinder -geometry 80x36 -fn 10x20 -bg gainsboro -fg slategray -e 'cd $HOME/docs; wordgrinder' &"
  #"Economy"       f.exec "gnumeric docs/ekonomi.xlsx &"
  #"FS-UAE"        f.exec "fs-uae-launcher &"
  #"File Manager"  f.exec "pcmanfm &"
  #""              f.nop
  #"Volume Mixer"  f.exec "xterm -e alsamixer --device pulse &"
  #"Audio Panel"   f.exec "pavucontrol &"
  "HTop"          f.exec "xterm -e htop &"
  ""              f.nop
  "Internet"      f.menu "internet"
  #"Graphics"      f.menu "gfx"
  #"Audio"         f.menu "audio"
  "Misc"          f.menu "miscapps"
}

menu "internet"
{
  "Firefox Nightly"     f.exec "firefox &"
  "Castor" f.exec "castor &"
  "IRC"  f.exec "uxterm -geometry 126x40 -T 'Internet Relay Chat' -e 'top' &"
}

# Workspace submenu: Graphics
#menu "gfx"
#{
#  ""          f.nop
#  #"Grafx2"    f.exec "grafx2&"
#  #"mtPaint"   f.exec "mtpaint&"
#}

# Workspace submenu: Misc apps
menu "miscapps"
{
   #"Falkon"      f.exec "falkon &"
   #"LeafPad"     f.exec "leafpad &"
   #"Geany"       f.exec "geany &"
   #"XPDF"        f.exec "xpdf &"
   #"Evince"      f.exec "evince &"
   #"FocusWriter" f.exec "focuswriter &"
   #"Abiword"     f.exec "abiword &"
   "Calculator"  f.exec "xcalc &"
   #"Economy"     f.exec "xterm -geometry 80x36 -title Economy -fn -*-fixed-medium-*-*-*-24-*-*-*-*-*-*-1 -e sc -n ~/docs/ekonomi &"
   #"Calendar"    f.exec "orage &"
   #"GNumeric"    f.exec "gnumeric &"
   "XMag"        f.exec "xmag -source 150x150 &"
   "XFontSel"    f.exec "xfontsel &"
   #"Dillo"       f.exec "dillo &"
}

# Workspace submenu: Audio apps
#menu "audio"
#{
#   ""              f.nop
#   #"Audacious"    f.exec "audacious &"
#   #"VLC"          f.exec "vlc &"
#   #"Gnome-MPV"    f.exec "gnome-mpv &"
#   #"MilkyTracker" f.exec "milkytracker -nosplash &"
#   #"MikMod"       f.exec "xterm -title MikMod -e mikmod &"
#}

# END twmrc
