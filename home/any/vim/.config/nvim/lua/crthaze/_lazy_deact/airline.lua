return {
	{
		"vim-airline/vim-airline",
		dependencies = {
			"vim-airline/vim-airline-themes",
		},
		init = function()
			vim.g.airline_theme = 'powerlineish'
			vim.g.airline_powerline_fonts = 1
			vim.g["airline#extensions#branch#enabled"] = 1
			vim.g["airline#extensions#ale#enabled"] = 1
			vim.g["airline#extensions#tabline#enabled"] = 1
			vim.g["airline#extensions#tagbar#enabled"] = 1
			vim.g.airline_skip_empty_sections = 1

			vim.g["airline#extensions#tabline#left_sep"] = ''
			vim.g["airline#extensions#tabline#left_alt_sep"] = ''

			-- powerline symbols
			vim.g.airline_left_sep = ''
			vim.g.airline_left_alt_sep = ''
			vim.g.airline_right_sep = ''
			vim.g.airline_right_alt_sep = ''
			-- vim.g.airline_symbols.branch = ''
			vim.g.airline_symbols["branch"] = ''
			vim.g.airline_symbols["readonly"] = ''
			vim.g.airline_symbols["linenr"] = ''
			vim.g.airline_symbols["notexists"] = ' 󱞄'
			vim.g.airline_symbols["dirty"] = '󱞂'
			vim.g.airline_symbols["crypt"] = '󰯄'
		end
	}
}
