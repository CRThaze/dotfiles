return {
	"akinsho/toggleterm.nvim",
	tag = "v2.11.0",
	init = function()
		require("toggleterm").setup()
		vim.keymap.set("n", "<leader>t", "<cmd>ToggleTerm direction=float<CR>")
	end
}
