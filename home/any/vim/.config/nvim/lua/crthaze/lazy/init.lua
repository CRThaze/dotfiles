return {
	"nvim-lua/plenary.nvim",
	"eandrju/cellular-automaton.nvim",
	"tpope/vim-commentary",
	"folke/todo-comments.nvim",
	"psliwka/vim-smoothie",
}
