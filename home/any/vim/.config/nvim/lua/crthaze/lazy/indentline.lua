return {
	{
		"Yggdroot/indentLine",
		init = function()
			vim.g.indentLine_enabled = 1
			vim.g.indentLine_concealcursor = ''
			vim.g.indentLine_char = '┆'
			vim.g.indentLine_faster = 1
			vim.g.indentLine_concealcursor = 'nc'
		end
	}
}
