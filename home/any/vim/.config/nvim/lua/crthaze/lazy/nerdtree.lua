return {
	{
		"scrooloose/nerdtree",
		init = function()
			vim.g.NERDTreeChDirMode = 2
			--vim.g.NERDTreeIgnore = {"node_modules", ".rbc$", "~$", ".pyc$", ".db$", ".sqlite$", "__pycache__"}
			--vim.g.NERDTreeSortOrder = {"^__.py$", "/$", "*", ".swp$", ".bak$", "~$"}
			vim.g.NERDTreeShowBookmarks = 1
			vim.g.NERDTreeMapOpenInTabSilent = '<RightMouse>'
			vim.g.NERDTreeWinSize = 50
			-- Open NERDTree on the right side of the window.
			vim.g.NERDTreeWinPos = "right"

			vim.opt.wildignore:append("*/tmp/*,*.so,*.swp,*.zip,*.pyc,*.db,*.sqlite,*node_modules/")

			vim.keymap.set("n", "<F2>", "<cmd>NERDTreeFind<CR>")
			vim.keymap.set("n", "<leader>n", "<cmd>NERDTreeFocus<CR>")
			vim.keymap.set("n", "<leader>e", "<cmd>NERDTreeToggle<CR>")
			vim.keymap.set("n", "<leader>o", "<cmd>NERDTreeFind<CR>")


			-- Start NERDTree when Vim starts with a directory argument.
			vim.api.nvim_create_autocmd(
				"StdinReadPre",
				{
					pattern = {"*"},
					callback = function()
						vim.s.std_in = 1
					end
				}
			)
			-- vim.api.nvim_create_autocmd(
			--	"VimEnter",
			--	{
			--		pattern = {"*"},
			--		callback = function()
			--			if argc() == 1 && isdirectory(argv()[0]) && !exists('s:std_in')
      --        execute 'NERDTree' argv()[0]
			--				wincmd p
			--				enew
			--				execute 'cd '.argv()[0]
			--			end
			--		end
			--	}
			--)
		end
	},
	{
		"jistr/vim-nerdtree-tabs",
		init = function()
			vim.g.nerdtree_tabs_focus_on_files = 1
		end
	}
}
