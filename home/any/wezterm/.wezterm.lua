-- Pull in the wezterm API
local wezterm = require 'wezterm'

-- This table will hold the configuration.
local config = {}

-- In newer versions of wezterm, use the config_builder which will
-- help provide clearer error messages
if wezterm.config_builder then
  config = wezterm.config_builder()
end

-- This is where you actually apply your config choices
config.keys = {
	{
		key = 'v',
		mods = 'CTRL|SHIFT|ALT',
		action = wezterm.action.SplitHorizontal { domain = 'CurrentPaneDomain' },
	},
	{
		key = 'h',
		mods = 'CTRL|SHIFT|ALT',
		action = wezterm.action.SplitVertical { domain = 'CurrentPaneDomain' },
	},
	{
		key = 'h',
		mods = 'CTRL|SHIFT',
		action = wezterm.action.ActivatePaneDirection "Left",
	},
	{
		key = 'l',
		mods = 'CTRL|SHIFT',
		action = wezterm.action.ActivatePaneDirection "Right",
	},
	{
		key = 'k',
		mods = 'CTRL|SHIFT',
		action = wezterm.action.ActivatePaneDirection "Up",
	},
	{
		key = 'j',
		mods = 'CTRL|SHIFT',
		action = wezterm.action.ActivatePaneDirection "Down",
	},
	{
		key = 'w',
		mods = 'CTRL|SHIFT',
		action = wezterm.action.CloseCurrentPane { confirm = true },
	},
}

config.font = wezterm.font 'Fira Code Nerd Font'

-- config.font_rules = {
-- 	-- For Bold-but-not-italic text, use this relatively bold font, and override
-- 	-- its color to a tomato-red color to make bold text really stand out.
-- 	{
-- 		intensity = 'Bold',
-- 		italic = false,
-- 		font = wezterm.font("FiraCode Nerd Font", {weight="Regular", stretch="Normal", style="Normal"}),
-- 	},

-- 	-- Bold-and-italic
-- 	{
-- 		intensity = 'Bold',
-- 		italic = true,
-- 		font = wezterm.font("FiraCode Nerd Font", {weight="Bold", stretch="Normal", style="Normal"}),
-- 	},

-- 	-- normal-intensity-and-italic
-- 	{
-- 		intensity = 'Normal',
-- 		italic = true,
-- 		font = wezterm.font("FiraCode Nerd Font", {weight="Regular", stretch="Normal", style="Normal"}),
-- 	},

-- 	-- half-intensity-and-italic (half-bright or dim); use a lighter weight font
-- 	{
-- 		intensity = 'Half',
-- 		italic = true,
-- 		font = wezterm.font("FiraCode Nerd Font", {weight="Light", stretch="Normal", style="Normal"}),
-- 	},

-- 	-- half-intensity-and-not-italic
-- 	{
-- 		intensity = 'Half',
-- 		italic = false,
-- 		font = wezterm.font("FiraCode Nerd Font", {weight="Light", stretch="Normal", style="Normal"}),
-- 	},
-- }

config.font_size = 10.0
config.color_scheme = 'Srcery (Gogh)'
config.colors = {
	cursor_bg = 'rgba(235,219,178,0)',
	cursor_fg = 'rgba(0,0,0,0)',
}
config.inactive_pane_hsb = {
	saturation = 0.8,
	brightness = 0.4,
}
config.window_background_opacity = 0.90
config.window_decorations = "INTEGRATED_BUTTONS|RESIZE"
config.use_fancy_tab_bar = false
config.tab_bar_at_bottom = true
config.hide_tab_bar_if_only_one_tab = true
config.default_cursor_style = "BlinkingBlock"

-- and finally, return the configuration to wezterm
return config
