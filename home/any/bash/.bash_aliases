if ! limited_terminal && [ -x "$(command -v tput)" ]
then
	alias reset='tput reset'
fi

# Git
alias git="GPG_TTY=\$(tty) git"
alias gitfixlast="git rebase -i \$(git log | grep '^commit ' | head -3 | tail -1 | cut -d' ' -f2) && git push -f"
alias gitcommitfix="git add -u && git commit -m f && gitfixlast"
alias gitpushnew="bash -c \"\$(git push 2>&1 | grep -P '^\s+git push' | perl -pe 's/^\s+//')\""

# KubeCtl
if [ -x "$(command -v kubectl)" ]
then
	alias kubectl-ctx="kubectl config get-contexts --no-headers | awk '{print \$2}' | xargs -I {} bash -c -- echo {} && kubectl config use-context"
	alias kubectl-ns="kubectl get namespaces --no-headers | awk '{print \$1}' | xargs -I {} bash -c -- echo {} && kubectl -n {}"
fi

alias k=kubectl

# Vim
if [ -x "$(command -v nvim)" ] && ! limited_terminal
then
	vim_path="$(command -v vim)"
	if [ -x "$vim_path" ]
	then
		alias vvim=$vim_path
		alias vim=nvim
		alias vimdiff="nvim -d"
	fi
fi

if [ -x "$(command -v neomutt)" ]
then
	alias mutt=neomutt
fi

# Terminal Screensaver
if [ -x "$(command -v cmatrix)" ]
then
	alias matrix="cmatrix -a -b"
	if limited_terminal
	then
		alias matrix="cmatrix -C white"
	fi
	alias screensaver="matrix -s"
	alias ss=screensaver
	alias lockscreen="screensaver && clear && exit"
	alias hardlockscreen="matrix -L"
	alias lock=lockscreen
fi

# Wezterm
if [ -x "$(command -v flatpak)" ]
then
	if [ -n "$(flatpak list --columns=application | grep org.wezfurlong.wezterm)" ]
	then
		alias wezterm='flatpak run org.wezfurlong.wezterm'
	fi
fi

if [ -x "$(command -v wezterm)" ]
then
	alias title='wezterm cli set-tab-title'
fi

# Local
if [ -f ~/.bash_aliases.local ]
then
	. "$HOME/.bash_aliases.local"
fi
