# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

#### Sanity Check ####
#######################

# If not running interactively, don't do anything
case $- in
	*i*)
		;;
	*)
		return
		;;
esac

#### Shell Options ####
########################
shopt -s extglob

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=4096
HISTFILESIZE=4096

# TERM
if [[ "$(basename "/"$(ps -o cmd -f -p $(cat /proc/$(echo $$)/stat | cut -d \  -f 4) | tail -1 | sed 's/ .*$//'))" == "cool-retro-term" ]]
then
	if [[ "$(ps -o cmd -f -p $(cat /proc/$(echo $$)/stat | cut -d \  -f 4) | tail -1 | perl -pe 's/^\S+( -p (\w+))?/\2/')" == "VT220" ]]
	then
		export TERM=vt220
		unset COLORTERM
	fi
fi

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

if [ -d "/opt/homebrew" ]
then
	eval "$(/opt/homebrew/bin/brew shellenv)"
fi

if [ -d "$HOME/.asdf" ]
then
	. $HOME/.asdf/asdf.sh
	. $HOME/.asdf/completions/asdf.bash
else
	if [ -d "$HOME/.pyenv" ]; then
		export PYENV_ROOT="$HOME/.pyenv"
		export PATH="$PYENV_ROOT/shims:$PATH"
		export PATH="$PYENV_ROOT/bin:$PATH"
		if command -v pyenv 1>/dev/null 2>&1; then
			if [[ "${OSTYPE%%+([[:digit:].])}" == "darwin" ]]
			then
				eval "$(pyenv init --path)"
			fi
			eval "$(pyenv init -)"
			eval "$(pyenv virtualenv-init -)"
		fi
	fi

	if [[ -s "$HOME/.gvm/scripts/gvm" ]]; then
		source "$HOME/.gvm/scripts/gvm"
	fi

	if [[ -d "$HOME/.fnm" ]]
	then
		export PATH=$HOME/.fnm:$PATH
		eval "`fnm env`"
		eval "`fnm completions --shell bash`"
	fi

	if [[ -d "$HOME/.rbenv" ]]
	then
		export PATH="$HOME/.rbenv/bin:$PATH"
		eval "$(rbenv init - bash)"
	fi

	if [[ -d "$HOME/.tfenv" ]]
	then
		export PATH="$HOME/.tfenv/bin:$PATH"
		eval "$(rbenv init - bash)"
	fi

	if [[ "$OSTYPE" == "linux-gnu"*  && -f /etc/debian_version ]]
	then
		# if [ -d $HOME/.pyenv ]
		# then
		# 	pyenv3
		# 	export HI="hi"
		# fi
		echo
	fi
fi

[ -d "$HOME/.modular" ] && export PATH="$PATH:$HOME/.modular/bin"

if [ -d "$HOME/.kube" ]
then
	export KUBECONFIG=$HOME/.kube/config.yaml
	source <(kubectl completion bash)
fi

if [ -d "$HOME/.local/bin" ]
then
  export PATH="$HOME/.local/bin:$PATH"
fi
if [ -d "$HOME/bin" ]
then
  export PATH="$HOME/bin:$PATH"
fi

if [ -d "$HOME/devel/go" ]
then
  export GOPATH="$HOME/devel/go"
  export PATH="$GOPATH/bin:$PATH"
fi

if [ -d "$HOME/devel/venvs" ]
then
	export WORKON_HOME=$HOME/devel/venvs
	wrapper_path="$(asdf where python)/bin/virtualenvwrapper.sh"
	if [ -f $wrapper_path ]
	then
		source $wrapper_path
	fi
fi

#### Includes ####
##################

# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -f ~/.bash_aliases ]
then
	. "$HOME/.bash_aliases"
fi

if [ -f ~/.bash_functions ]
then
	. "$HOME/.bash_functions"
fi

# Machine Specific Configs
if [ -f ~/.bashrc.local ]
then
	. "$HOME/.bashrc.local"
fi

####

[[ -f ~/.bash-preexec.sh ]] && source ~/.bash-preexec.sh

if [ -d ~/.atuin ]
then
	source "$HOME/.atuin/bin/env"
	eval "$(atuin init bash --disable-up-arrow)"
	bind -x '"\C-r": __atuin_history'
fi
