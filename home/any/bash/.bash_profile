# ~/.bash_profile

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/bin" ]; then
  export PATH="$HOME/bin:$PATH"
fi

# [ -d "$HOME/devel/venvs" ] && export WORKON_HOME=~/devel/venvs
# [ -d "$HOME/devel/go" ] && export GOPATH=$HOME/devel/go
# [ -d /usr/local/go/bin ] && export PATH=/usr/local/go/bin:$PATH
# [ -d "$HOME/.local/bin" ] && export PATH=$HOME/.local/bin:$PATH

if command -v go 1>/dev/null 2>&1
then
	export PATH=$PATH:$(go env GOPATH)/bin
fi

if [[ "$OSTYPE" == "linux-gnu"*  && ! -f /etc/debian_version ]]
then
	echo
fi

[ -f ~/.bash_profile.local ] && . "$HOME/.bash_profile.local"

[ -f "$HOME/.bashrc" ] && . "$HOME/.bashrc"

. "$HOME/.atuin/bin/env"
