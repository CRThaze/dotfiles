#!/bin/bash

# Test for 256 color support
test_256color() {
    echo -e "\e[48;5;82mTesting 256 colors...\e[0m"
    if [ $? -eq 0 ]; then
        echo "256-color support detected"
        return 0
    else
        echo "256-color support not detected"
        return 1
    fi
}

# Test for cursor movement
test_cursor_movement() {
    echo -e "\e[s\e[10;10H\e[u"  # Save cursor position, move to 10,10, and restore
    if [ $? -eq 0 ]; then
        echo "Cursor movement support detected"
        return 0
    else
        echo "Cursor movement support not detected"
        return 1
    fi
}

# Test for alternate screen buffer
test_alternate_buffer() {
    echo -e "\e[?1049h"  # Enter alternate screen buffer
    echo -e "\e[?1049l"  # Exit alternate screen buffer
    if [ $? -eq 0 ]; then
        echo "Alternate screen buffer support detected"
        return 0
    else
        echo "Alternate screen buffer support not detected"
        return 1
    fi
}

# Test for mouse support
test_mouse_support() {
    echo -e "\e[?1000h"  # Enable mouse tracking
    echo -e "\e[?1000l"  # Disable mouse tracking
    if [ $? -eq 0 ]; then
        echo "Mouse support detected"
        return 0
    else
        echo "Mouse support not detected"
        return 1
    fi
}

# Run tests
test_256color
test_cursor_movement
test_alternate_buffer
test_mouse_support

