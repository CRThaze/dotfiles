#!/bin/bash

ROLE_ARN=$1
SESSION_NAME=$2

unset AWS_ACCESS_KEY_ID
unset AWS_SECRET_ACCESS_KEY
unset AWS_SESSION_TOKEN

resp=$( \
	aws \
		sts \
		assume-role \
		--role-arn $ROLE_ARN \
		--role-session-name $SESSION_NAME \
		--duration-seconds 43200 \
)

# echo $resp

export AWS_ACCESS_KEY_ID="$(echo $resp | jq -r .Credentials.AccessKeyId)"
export AWS_SECRET_ACCESS_KEY="$(echo $resp | jq -r .Credentials.SecretAccessKey)"
export AWS_SESSION_TOKEN="$(echo $resp | jq -r .Credentials.SessionToken)"
