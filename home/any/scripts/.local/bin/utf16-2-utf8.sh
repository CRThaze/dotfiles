#!/bin/bash
printf "$(python -c "print('$1'.encode('utf-16', 'surrogatepass').decode('utf-16').encode('utf-8'))" | perl -pe "s/(^b|')//g")"
