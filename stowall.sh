#!/usr/bin/env bash

shopt -s extglob
for x in home/{any,"${OSTYPE%%+([[:digit:].])}"}/*
do
	if [[ -f ${x}.dirs ]]
	then
		cat ${x}.dirs | xargs -I {} mkdir -p $HOME/{}
	fi
	if [[ -d $x ]]
	then
		stow -t $HOME -d $(dirname $x) $(basename $x)
	fi
done

chmod 700 $HOME/.gnupg

sudo chown root:root -R etc/*/*/

for x in etc/{any,"${OSTYPE%%+([[:digit:].])}"}/*
do
	if [[ -f ${x}.dirs ]]
	then
		cat ${x}.dirs | xargs -I {} sudo mkdir -p /etc/{}
	fi
	if [[ -d $x ]]
	then
		sudo stow -t /etc -d $(dirname $x) $(basename $x)
	fi
done
