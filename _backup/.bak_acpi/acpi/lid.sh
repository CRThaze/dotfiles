#!/bin/sh

state=$(echo "$1" | cut -d " " -f 3)
case "$state" in
	open)
		# do what needs to be done when the lid is opened
		if ! xbanish_loc="$(type -p "xbanish")" || [[ -z $xbanish_loc ]]
		then
			killall xbanish
			xbanish &
		fi
		;;
	close)
		# do what needs to be done when the lid is closed
		;;
	*)
		# panic: not a state I know about!
esac

