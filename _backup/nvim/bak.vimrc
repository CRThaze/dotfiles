call plug#begin('~/.vim/plugged')
if empty($VIM_LITE)
	Plug 'fatih/vim-go', { 'do': ':GoUpdateBinaries' } " Amazing combination of features.
endif
Plug 'jmcantrell/vim-virtualenv'
" Plug 'mg979/vim-visual-multi'
Plug 'airblade/vim-rooter'
if empty($VIM_LITE)
	Plug 'vim-airline/vim-airline'
	Plug 'vim-airline/vim-airline-themes'
	Plug 'majutsushi/tagbar' " install exuberant-ctags
endif
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-fugitive'
Plug 'airblade/vim-gitgutter'
" Plug 'SirVer/ultisnips'
" Plug 'neomake/neomake'
" Plug 'godlygeek/tabular'
" Plug 'lepture/vim-jinja'
" Plug 'b4b4r07/vim-hcl'
" Plug 'hashivim/vim-packer'
" Plug 'hashivim/vim-terraform'
" Plug 'scrooloose/nerdtree'
Plug 'preservim/nerdtree'
Plug 'ryanoasis/vim-devicons'
if empty($VIM_LITE)
	Plug 'towolf/vim-helm'
	Plug 'ARM9/arm-syntax-vim'
endif
" Plug 'PotatoesMaster/i3-vim-syntax'
"Plug 'tpope/vim-surround'
"Plugin 'jiangmiao/auto-pairs'
" Plug 'tsandall/vim-rego'
" Plug 'honza/vim-snippets'
if empty($VIM_LITE)
	Plug 'psliwka/vim-smoothie'
endif
Plug 'Yggdroot/indentLine'
if !has('nvim')
	" Plugins for when you're not using neovim
endif
if has('nvim')
	Plug 'nvim-lua/plenary.nvim'
	Plug 'nvim-telescope/telescope.nvim', { 'tag': '0.1.8' }
	" Plug 'neoclide/coc.nvim', {'do': 'yarn install --frozen-lockfile'}
	Plug 'neoclide/coc.nvim', {'branch': 'release'}
	"Plug 'jodosha/vim-godebug' " Debugger integration via delve
	Plug 'github/copilot.vim'
	" Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
	Plug 'folke/todo-comments.nvim'
	Plug 'akinsho/toggleterm.nvim', {'tag' : 'v2.11.0'}
	Plug 'neovim/nvim-lspconfig'
	" Plug 'mfussenegger/nvim-dap-python'
	" Plug 'linux-cultist/venv-selector.nvim', {'branch': 'regexp'}
endif
" All of your Plugs must be added before the following line
call plug#end()

"""""""""""""""""
" Plugin Config "
"""""""""""""""""
let mapleader = " "

""" coc
if has('nvim')
	" ---------------------------------------------------------------------------
	" coc.nvim default settings
	" ---------------------------------------------------------------------------
	let g:coc_global_extensions = [
		\'coc-json',
		\'coc-go',
		\'@yaegassy/coc-pylsp',
		\'coc-pydocstring',
		\'coc-yaml',
		\'coc-snippets',
		\'coc-lua',
		\'coc-solargraph',
		\'coc-toml',
		\'coc-docker',
		\'coc-clangd',
		\'coc-tsserver',
		\'coc-html'
	\]

	" Some servers have issues with backup files, see #649
	" set nobackup
	" set nowritebackup

	" Having longer updatetime (default is 4000 ms = 4s) leads to noticeable
	" delays and poor user experience
	set updatetime=300

	" Always show the signcolumn, otherwise it would shift the text each time
	" diagnostics appear/become resolved
	" set signcolumn=yes

	" Make <CR> to accept selected completion item or notify coc.nvim to format
	" <C-g>u breaks current undo, please make your own choice
	inoremap <silent><expr> <CR> coc#pum#visible() ? coc#pum#confirm()
																\: "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"

	" function! CheckBackspace() abort
	"		let col = col('.') - 1
	"		return !col || getline('.')[col - 1]  =~# '\s'
	" endfunction

	" Use `[g` and `]g` to navigate diagnostics
	" Use `:CocDiagnostics` to get all diagnostics of current buffer in location list
	nmap <silent> [g <Plug>(coc-diagnostic-prev)
	nmap <silent> ]g <Plug>(coc-diagnostic-next)

	" GoTo code navigation
	nmap <silent> gd <Plug>(coc-definition)
	nmap <silent> gy <Plug>(coc-type-definition)
	nmap <silent> gi <Plug>(coc-implementation)
	nmap <silent> gr <Plug>(coc-references)

	" Use K to show documentation in preview window
	nnoremap <silent> K :call ShowDocumentation()<CR>

	function! ShowDocumentation()
		if CocAction('hasProvider', 'hover')
			call CocActionAsync('doHover')
		else
			call feedkeys('K', 'in')
		endif
	endfunction

	" Highlight the symbol and its references when holding the cursor
	autocmd CursorHold * silent call CocActionAsync('highlight')

	" Symbol renaming
	nmap <leader>rn <Plug>(coc-rename)

	" Formatting selected code
	xmap <leader>f  <Plug>(coc-format-selected)
	nmap <leader>f  <Plug>(coc-format-selected)

	augroup mygroup
		autocmd!
		" Setup formatexpr specified filetype(s)
		autocmd FileType typescript,json setl formatexpr=CocAction('formatSelected')
		" Update signature help on jump placeholder
		autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')
	augroup end

	" Applying code actions to the selected code block
	" Example: `<leader>aap` for current paragraph
	xmap <leader>a  <Plug>(coc-codeaction-selected)
	nmap <leader>a  <Plug>(coc-codeaction-selected)

	" Remap keys for applying code actions at the cursor position
	nmap <leader>ac  <Plug>(coc-codeaction-cursor)
	" Remap keys for apply code actions affect whole buffer
	nmap <leader>as  <Plug>(coc-codeaction-source)
	" Apply the most preferred quickfix action to fix diagnostic on the current line
	nmap <leader>qf  <Plug>(coc-fix-current)

	" Remap keys for applying refactor code actions
	nmap <silent> <leader>re <Plug>(coc-codeaction-refactor)
	xmap <silent> <leader>r  <Plug>(coc-codeaction-refactor-selected)
	nmap <silent> <leader>r  <Plug>(coc-codeaction-refactor-selected)

	" Run the Code Lens action on the current line
	nmap <leader>cl  <Plug>(coc-codelens-action)

	" Map function and class text objects
	" NOTE: Requires 'textDocument.documentSymbol' support from the language server
	xmap if <Plug>(coc-funcobj-i)
	omap if <Plug>(coc-funcobj-i)
	xmap af <Plug>(coc-funcobj-a)
	omap af <Plug>(coc-funcobj-a)
	xmap ic <Plug>(coc-classobj-i)
	omap ic <Plug>(coc-classobj-i)
	xmap ac <Plug>(coc-classobj-a)
	omap ac <Plug>(coc-classobj-a)

	" Remap <C-f> and <C-b> to scroll float windows/popups
	" if has('nvim-0.4.0') || has('patch-8.2.0750')
	"		nnoremap <silent><nowait><expr> <C-f> coc#float#has_scroll() ? coc#float#scroll(1) : "\<C-f>"
	"		nnoremap <silent><nowait><expr> <C-b> coc#float#has_scroll() ? coc#float#scroll(0) : "\<C-b>"
	"		inoremap <silent><nowait><expr> <C-f> coc#float#has_scroll() ? "\<c-r>=coc#float#scroll(1)\<cr>" : "\<Right>"
	"		inoremap <silent><nowait><expr> <C-b> coc#float#has_scroll() ? "\<c-r>=coc#float#scroll(0)\<cr>" : "\<Left>"
	"		vnoremap <silent><nowait><expr> <C-f> coc#float#has_scroll() ? coc#float#scroll(1) : "\<C-f>"
	"		vnoremap <silent><nowait><expr> <C-b> coc#float#has_scroll() ? coc#float#scroll(0) : "\<C-b>"
	" endif
	
	" Code Gen
	nmap <silent> ga <Plug>(coc-codeaction-line)
	xmap <silent> ga <Plug>(coc-codeaction-selected)
	nmap <silent> gA <Plug>(coc-codeaction)

	" Use CTRL-S for selections ranges
	" Requires 'textDocument/selectionRange' support of language server
	nmap <silent> <C-s> <Plug>(coc-range-select)
	xmap <silent> <C-s> <Plug>(coc-range-select)

	" Add `:Format` command to format current buffer
	command! -nargs=0 Format :call CocActionAsync('format')

	" Add `:Fold` command to fold current buffer
	command! -nargs=? Fold :call     CocAction('fold', <f-args>)

	" Add `:OR` command for organize imports of the current buffer
	command! -nargs=0 OR   :call     CocActionAsync('runCommand', 'editor.action.organizeImport')

	" Add (Neo)Vim's native statusline support
	" NOTE: Please see `:h coc-status` for integrations with external plugins that
	" provide custom statusline: lightline.vim, vim-airline

	" Mappings for CoCList
	" Show all diagnostics
	nnoremap <silent><nowait> <space>a  :<C-u>CocList diagnostics<cr>
	" Manage extensions
	nnoremap <silent><nowait> <space>e  :<C-u>CocList extensions<cr>
	" Show commands
	nnoremap <silent><nowait> <space>c  :<C-u>CocList commands<cr>
	" Find symbol of current document
	nnoremap <silent><nowait> <space>o  :<C-u>CocList outline<cr>
	" Search workspace symbols
	nnoremap <silent><nowait> <space>s  :<C-u>CocList -I symbols<cr>
	" Do default action for next item
	nnoremap <silent><nowait> <space>j  :<C-u>CocNext<CR>
	" Do default action for previous item
	nnoremap <silent><nowait> <space>k  :<C-u>CocPrev<CR>
	" Resume latest coc list
	nnoremap <silent><nowait> <space>p  :<C-u>CocListResume<CR>

	"""" Snippets

	" Use <C-l> for trigger snippet expand.
	imap <C-l> <Plug>(coc-snippets-expand)

	" Use <C-j> for select text for visual placeholder of snippet.
	vmap <C-j> <Plug>(coc-snippets-select)

	" Use <C-j> for jump to next placeholder, it's default of coc.nvim
	let g:coc_snippet_next = '<c-j>'

	" Use <C-k> for jump to previous placeholder, it's default of coc.nvim
	let g:coc_snippet_prev = '<c-k>'

	" Use <C-j> for both expand and jump (make expand higher priority.)
	imap <C-j> <Plug>(coc-snippets-expand-jump)

	" Use <leader>x for convert visual selected code to snippet
	xmap <leader>x  <Plug>(coc-convert-snippet)

	" inoremap <silent><expr> <TAB>
	"		\ pumvisible() ? coc#_select_confirm() :
	"		\ coc#expandableOrJumpable() ? "\<C-r>=coc#rpc#request('doKeymap', ['snippets-expand-jump',''])\<CR>" :
	"		\ <SID>check_back_space() ? "\<TAB>" :
	"		\ coc#refresh()

	" function! s:check_back_space() abort
	"		let col = col('.') - 1
	"		return !col || getline('.')[col - 1]  =~# '\s'
	" endfunction

	" let g:coc_snippet_next = '<tab>'
endif


""" tagbar
if empty($VIM_LITE)
	nmap <leader>b :TagbarToggle<CR>
	" let g:tagbar_type_go = {
	"     \ 'ctagstype' : 'go',
	"     \ 'kinds'     : [
	"         \ 'p:package',
	"         \ 'i:imports:1',
	"         \ 'c:constants',
	"         \ 'v:variables',
	"         \ 't:types',
	"         \ 'n:interfaces',
	"         \ 'w:fields',
	"         \ 'e:embedded',
	"         \ 'm:methods',
	"         \ 'r:constructor',
	"         \ 'f:functions'
	"     \ ],
	"     \ 'sro' : '.',
	"     \ 'kind2scope' : {
	"         \ 't' : 'ctype',
	"         \ 'n' : 'ntype'
	"     \ },
	"     \ 'scope2kind' : {
	"         \ 'ctype' : 't',
	"         \ 'ntype' : 'n'
	"     \ },
	"     \ 'ctagsbin'  : 'gotags',
	"     \ 'ctagsargs' : '-sort -silent'
	" \ }
endif


""" vim-rooter
" let g:rooter_use_lcd = 1
let g:rooter_cd_cmd="lcd"
let g:rooter_silent_chdir = 1
let g:rooter_resolve_links = 1


""" vim-terraform
" let g:terraform_fmt_on_save=1
"autocmd filetype tf set syntax=terraform
" autocmd FileType terraform setlocal commentstring=#%s


""" vim-jinja
" autocmd BufNewFile,BufRead *.j2 set filetype=jinja


""" vim-go
" " disable all linters as that is taken care of by coc.nvim
" let g:go_diagnostics_enabled = 0
" let g:go_metalinter_enabled = []

" " don't jump to errors after metalinter is invoked
" let g:go_jump_to_error = 0

" " run go imports on file save
" let g:go_fmt_command = "gofmt"

" " automatically highlight variable your cursor is on
" let g:go_auto_sameids = 0

" let g:go_highlight_types = 1
" let g:go_highlight_types = 1
" let g:go_highlight_fields = 1
" let g:go_highlight_functions = 1
" let g:go_highlight_function_calls = 1
" let g:go_highlight_operators = 1
" let g:go_highlight_extra_types = 1
" let g:go_highlight_build_constraints = 1
" let g:go_highlight_generate_tags = 1

" " let g:go_def_mode='gopls'
" " let g:go_info_mode='gopls'
" let g:go_term_enabled = 1

" " disable vim-go :GoDef short cut (gd)
" " this is handled by LanguageClient [LC]
" let g:go_def_mapping_enabled = 0


""" neomake
" Full config: when writing or reading a buffer, and on changes in insert and
" normal mode (after 1s; no delay when writing).
" call neomake#configure#automake('nrwi', 500)


""" NERDtree
nnoremap <leader>n :NERDTreeFocus<CR>
nnoremap <leader>e :NERDTreeToggle<CR>
nnoremap <leader>o :NERDTreeFind<CR>

" Open NERDTree on the right side of the window.
let g:NERDTreeWinPos = "right"

" Start NERDTree when Vim starts with a directory argument.
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 1 && isdirectory(argv()[0]) && !exists('s:std_in') |
    \ execute 'NERDTree' argv()[0] | wincmd p | enew | execute 'cd '.argv()[0] | endif
" Start NERDTree when Vim is started without file arguments.
autocmd VimEnter * if argc() == 0 && !exists('s:std_in') | NERDTree | endif
" Exit Vim if NERDTree is the only window remaining in the only tab.
autocmd BufEnter * if tabpagenr('$') == 1 && winnr('$') == 1 && exists('b:NERDTree') && b:NERDTree.isTabTree() | quit | endif
" Close the tab if NERDTree is the only window remaining in it.
autocmd BufEnter * if winnr('$') == 1 && exists('b:NERDTree') && b:NERDTree.isTabTree() | quit | endif
" If another buffer tries to replace NERDTree, put it in the other window, and bring back NERDTree.
autocmd BufEnter * if bufname('#') =~ 'NERD_tree_\d\+' && bufname('%') !~ 'NERD_tree_\d\+' && winnr('$') > 1 |
    \ let buf=bufnr() | buffer# | execute "normal! \<C-W>w" | execute 'buffer'.buf | endif



""" airline
let g:airline_theme='powerlineish' " <theme> is a valid theme name
let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#formatter = 'unique_tail_improved'
let g:airline#extensions#virtualenv#enabled = 1
let g:airline#extensions#virtualenv#ft = ['python', 'markdown', 'dosini', 'text', 'make']

" customize the statusline.
" let g:airline_section_w=%{virtualenv#statusline()}
" let g:airline_section_w = %{coc#status()}%{get(b:,'coc_current_function','')}

""" Telescope
" Find files using Telescope command-line sugar.
if has('nvim')
	nnoremap <leader>ff <cmd>Telescope find_files<cr>
	nnoremap <leader>fg <cmd>Telescope live_grep<cr>
	nnoremap <leader>fb <cmd>Telescope buffers<cr>
	nnoremap <leader>fh <cmd>Telescope help_tags<cr>
endif

""" TreeSitter
" if has('nvim')
" 	luafile ~/.vim/vimrc.d/treesitter.lua
" endif

""" todo-comments
if has('nvim')
	luafile ~/.vim/vimrc.d/todo-comments.lua
endif

""" toggleterm
if has('nvim')
	lua require("toggleterm").setup()
	nnoremap <leader>t :ToggleTerm direction=float<CR>
	
endif

""" venv-selector
" luafile ~/.vim/vimrc.d/venv-selector.lua
" nnoremap <leader>vs :VenvSelect<CR>


""" vim-smoothie

"""""""""""""""""""""
" Personal Settings "
"""""""""""""""""""""
set number

colorscheme Czar
hi Normal guibg=NONE ctermbg=NONE

command! W w !sudo tee >/dev/null %

set backupdir=~/.vim/tmp/backup//
set directory=~/.vim/tmp/swap//
set undodir=~/.vim/tmp/undo//

set completeopt-=preview

"set list lcs=tab:\┆\ ,trail:▓,space:.
"set list lcs=tab:\⇥\ ,trail:▓,space:.
set list lcs=tab:\⇥\ ,trail:▓

set colorcolumn=80
autocmd Filetype java setlocal colorcolumn=100
autocmd Filetype html setlocal colorcolumn=100
autocmd Filetype go setlocal colorcolumn=100
autocmd Filetype netrw setlocal colorcolumn=
autocmd Filetype netrw setlocal signcolumn=0

set tabstop=2
set shiftwidth=2
set noexpandtab
autocmd FileType python setlocal expandtab
autocmd FileType python setlocal tabstop=4
autocmd FileType python setlocal shiftwidth=4
" autocmd filetype json setlocal expandtab
autocmd filetype tf setlocal expandtab
autocmd filetype terraform setlocal expandtab
autocmd FileType terraform setlocal tabstop=4
autocmd FileType terraform setlocal shiftwidth=4
autocmd FileType helm setlocal expandtab

let g:indentLine_concealcursor = 'nc'

""" Help Navigation
" autocmd FileType help nnoremap <buffer> <CR> <C-]>
" autocmd FileType help nnoremap <buffer> <BS> <C-T>
" autocmd FileType help nnoremap <buffer> o /'\l\{2,\}'<CR>
" autocmd FileType help nnoremap <buffer> O ?'\l\{2,\}'<CR>
" autocmd FileType help nnoremap <buffer> s /\|\zs\S\+\ze\|<CR>
" autocmd FileType help nnoremap <buffer> S ?\|\zs\S\+\ze\|<CR>
