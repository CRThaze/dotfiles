local function on_venv_activate()
	local command_run = false

	local function run_shell_command()
		local venv = require("venv-selector").venv():match("([^/]+)$")

		if command_run == false then
			local command = "workon " .. venv
			vim.api.nvim_feedkeys(command .. "\n", "n", false)
			command_run = true
		end
	end

	vim.api.nvim_create_augroup("TerminalCommands", { clear = true })

	vim.api.nvim_create_autocmd("TermEnter", {
		group = "TerminalCommands",
		pattern = "*",
		callback = run_shell_command,
	})
	vim.print("Activated venv: " .. require("venv-selector").venv())
end

require("venv-selector").setup {
	settings = {
		options = {
			on_venv_activate_callback = on_venv_activate,
			notify_user_on_venv_activation = true,
		},
		search = {
			my_venvs = {
				command = "ls -d ~/devel/venvs/*/",
			},
		},
	},
}

