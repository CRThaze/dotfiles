function! myspacevim#before() abort
	" let g:go_gopls_enabled = 0
	" let g:neomake_c_enabled_makers = ['clang']
	" you can defined mappings in bootstrap function
	" for example, use kj to exit insert mode.
	" inoremap kj <Esc>
	set colorcolumn=80
	autocmd Filetype java setlocal colorcolumn=100
	autocmd Filetype html setlocal colorcolumn=100
	autocmd Filetype go setlocal colorcolumn=100
	autocmd Filetype netrw setlocal colorcolumn=

	autocmd FileType python setlocal expandtab
	autocmd FileType python setlocal tabstop=4
	autocmd FileType python setlocal shiftwidth=4
	autocmd FileType yaml setlocal expandtab
	autocmd FileType helm setlocal expandtab

	command! W w !sudo tee >/dev/null %
endfunction

function! myspacevim#after() abort
	" you can remove key binding in bootstrap_after function
	" iunmap kj
	let g:neomake_go_enabled_makers = []
	hi Normal guibg=NONE ctermbg=NONE
	hi EndOfBuffer guibg=NONE ctermbg=NONE
	" let g:coc_global_extensions = [
	" 	\'coc-json',
	" 	\'coc-go',
	" 	\'@yaegassy/coc-pylsp',
	" 	\'coc-yaml',
	" 	\'coc-snippets',
	" 	\'coc-lua',
	" 	\'coc-solargraph',
	" 	\'coc-toml',
	" 	\'coc-docker',
	" 	\'coc-clangd',
	" 	\'coc-tsserver',
	" 	\'coc-html'
	" \]
endfunction
