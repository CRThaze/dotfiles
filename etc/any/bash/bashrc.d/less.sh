# Enable Source Highlighting in less
LESSPIPE=/usr/bin/src-hilite-lesspipe.sh
if [ -x $LESSPIPE ]
then
  export LESSOPEN="| ${LESSPIPE} %s"
  export LESS=' -R '
fi
