BASE_COLOR=4
[ "$(id -u)" -eq 0 ] && BASE_COLOR=1

REMOTE=false
if [ -n "$SSH_CLIENT" ] || [ -n "$SSH_TTY" ]; then
	REMOTE=true
else
	case $(ps -o comm= -p $PPID) in
		sshd|*/sshd) REMOTE=true;;
	esac
fi

if [ $REMOTE = true ]
then
	if [ -f /etc/nickname ]
	then
		NICK="$(cat /etc/nickname | tr '[:lower:]' '[:upper:]' | xargs echo)"
	else
		NICK="SSH"
	fi
	PREFIX="\[$(tput setaf $BASE_COLOR)\]\[$(tput bold)\][\[$(tput setaf 1)\]$NICK\[$(tput setaf $BASE_COLOR)\]]\[$(tput sgr0)\]"
else
	PREFIX=
fi

prompt_cmd() {
	EXITSTATUS=$?
	tput bold
	tput setaf $BASE_COLOR
	echo -n "["
	tput setaf 3
	echo -n "$(date -u +%H:%M:%S) UTC"
	tput setaf $BASE_COLOR
	echo -n "]"
	k8scontext=$(kubectl config current-context 2>/dev/null | perl -pe 's/\n$//')
	if [ ! -z "$k8scontext" ]
	then
		tput setaf $BASE_COLOR
		echo -n "["
		tput setaf 2
		echo -n "K8s: "
		tput setaf 1
		echo -n "$k8scontext"
		tput setaf $BASE_COLOR
		echo -n "]"
	fi
	awsprofile=$(aws-creds 2>/dev/null)
	if [ ! -z "$awsprofile" ]
	then
		tput setaf $BASE_COLOR
		echo -n "["
		tput setaf 2
		echo -n "AWS: "
		tput setaf 1
		echo -n "$awsprofile"
		tput setaf $BASE_COLOR
		echo -n "]"
	fi
	tput setaf 2
	echo -n "["
	if [[ $EXITSTATUS -eq 0 ]]
	then
		tput setaf 2
	else
		tput setaf 1
	fi
	echo -n "$EXITSTATUS"
	tput setaf 2
	echo -n "]"
	branch=$(hg branch 2>/dev/null || git branch 2>/dev/null | grep "\*" | perl -pe 's/\* ([^(]\S+)|\* \(HEAD detached at (\S+)\)/\1\2/')
	if [ ! -z "$branch" ]
	then
		echo
		tput setaf $BASE_COLOR
		echo -n "["
		tput setaf 1
		echo -n "$branch"
		tput setaf $BASE_COLOR
		echo -n "]"
	fi
	tput sgr0
	echo
}

if ! limited_terminal
then
	export PROMPT_COMMAND="prompt_cmd"
	export PS1="$PREFIX\[$(tput setaf $BASE_COLOR)\]\[$(tput bold)\][\[$(tput setaf 1)\]\u\[$(tput setaf $BASE_COLOR)\]@\h \[$(tput setaf 3)\]\W\[$(tput setaf $BASE_COLOR)\]]\[$(tput setaf 1)\]\\$ \[$(tput sgr0)\]"
fi
